# P2.2 - High Performance Computing Technologies
### Lecturers: Federico Vagnini, Giorgio Richelli, Walter Bernocchi


## Day1
- Introduction to OpenPOWER

## Day2
- Introduction to Machine Learning. If you want to play with **Tensorflow** follow these [instructions](./tensorflow/README.md).
- HPC from IBM's view point

## Day3
- setup VPN client: please refer to this [file](./vpn/README.md).
